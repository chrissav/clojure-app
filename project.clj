(defproject helloworld "1.0.0"
  :description "Hello World - Clojure test app"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [ring/ring-core "1.6.3"]
                 [ring/ring-jetty-adapter "1.6.3"]]
  :main helloworld.core)
